package kanchaistudio.studentcheckin;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.CountDownTimer;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import kanchaistudio.studentcheckin.R;
import kanchaistudio.studentcheckin.SettingActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.util.Log;

import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.sql.Time;
import java.util.Calendar;
import android.widget.DigitalClock;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;


class TheTask extends AsyncTask<String,String,String>
{   public AsyncResponse delegate=null;
	public interface AsyncResponse {
		void processFinish(String output);
	}

	@Override
	protected void onPostExecute(String result) {
		delegate.processFinish(result);
	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
	}

	@Override
	protected String doInBackground(String... params) {
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
		nameValuePairs.add(new BasicNameValuePair("studata",params[1]));
//		try {
//			UrlEncodedFormEntity entity = new UrlEncodedFormEntity(nameValuePairs);
//			httppost.addHeader(entity.getContentType());
//			httppost.setEntity(entity);
//		} catch (UnsupportedEncodingException e) {
//			Log.d("error", "UnsupportedEncodingException");
//		}
//		try {
//			// Execute HTTP Post Request
//			HttpResponse response = httpclient.execute(httppost);
//			_trySend = 0;
//		} catch (ClientProtocolException e) {
//			if( _trySend != 0)
//			{
//				sendTime();
//				_trySend--;
//			}
//		} catch (IOException e) {
//			if( _trySend != 0)
//			{
//				sendTime();
//				_trySend--;
//			}
//		}
//
		try
		{
			UrlEncodedFormEntity entity = new UrlEncodedFormEntity(nameValuePairs);
			HttpClient httpclient = new DefaultHttpClient();
			HttpPost method = new HttpPost(params[0]);
			method.setEntity(entity);

			HttpResponse response = httpclient.execute(method);
			HttpEntity result = response.getEntity();
			if(result != null){
				return EntityUtils.toString(result);
			}
			else{
				return "No string.";
			}
		}
		catch(Exception e){
			return "Network problem";
		}

	}
}


public class Main extends Activity implements TheTask.AsyncResponse {
	TheTask asyncTask = null;
	private final String _factoryReset = "*536*";
	private final String _resetCode = "*533*";
	private final Integer _defaultTerminalId = 0;
	private final Integer _defaultMaxDigit = 5;
	private final String _defaultSecretCode = "*999*";
	private final String _defaultManualSendCode = "*998*";
	private final String _defaultManualClearData = "*997*";
	private final Integer _defaultAutoSaveTime = 10;
	private final String _defaultServerPath = "";
	private BroadcastReceiver _broadcastReceiver = null;
	private final SimpleDateFormat _sdfWatchTime = new SimpleDateFormat("cccc d MMMM yyyy,  HH:mm");
	private TextView _txtShowSID = null;
	private TextView _txtTotalRecord = null;
	private TextView _txtCurrentDate = null;
	private CountDownTimer _autoSave = null;

	private final long _countDownInterval = 1000;
	private Timer _autoSend = null;
	private Timer _autoSend2 = null;
	private Timer _autoSend3 = null;
	//private ArrayList<Map> _data;
	private Set<String> _data;
	private boolean _shock = false;
	private TextView _txtCurrentTime = null;
	final String _keyPrefix = "kanchai.studentcheckin.app";
	private SharedPreferences _prefs;
	/* customize variables */
	private Date _autoSendTime = null;
	private Date _autoSendTime2 = null;
	private Date _autoSendTime3 = null;

	private String _serverPath;
	private int _maxDigit;
	private String _secretCode;
	private String _manualSendCode;
	private String _manualClearData;
	private int _terminalId;
	private int _autoSaveCountDown;
	private SimpleDateFormat stf = new SimpleDateFormat("HH:mm:ss");
	private SimpleDateFormat sdf = new SimpleDateFormat("cccc dd MMMM yyyy");
	private final Handler mHandler = new Handler();
	private boolean mActive;
	private int _trySend = 0;
	private final int _maxTrySend = 3;
	private final Runnable mRunnable = new Runnable() {
		public void run() {
			if (mActive) {
				if (_txtCurrentTime != null) {
					_txtCurrentTime.setText(getTime());
				}
				mHandler.postDelayed(mRunnable, 1000);
			}
		}
	};
	private Runnable AutoSendTimeTask = new Runnable() {
		public void run() {
			sendTime();
			Calendar calendar = Calendar.getInstance();
			Log.d("test", "started at "
							+ calendar.get(Calendar.HOUR_OF_DAY) + " "
							+ calendar.get(Calendar.MINUTE) + " "
							+ calendar.get(Calendar.SECOND)
			);
		}
	};
	private void startClock() {
		mActive = true;
		mHandler.post(mRunnable);
	}

    @Override
    protected void onCreate(Bundle savedInstanceState) {
	    _prefs = this.getSharedPreferences(_keyPrefix, Context.MODE_PRIVATE);
        super.onCreate(savedInstanceState);
	    Log.i("process","Activity created");
	    //Remove title bar
	    this.requestWindowFeature(Window.FEATURE_NO_TITLE);
	    //Remove notification bar
	    this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
	    //set content view AFTER ABOVE sequence (to avoid crash)
        setContentView(R.layout.activity_main);
	    _txtShowSID = (TextView)findViewById(R.id.txtSID);
	    _txtCurrentTime = (TextView)findViewById(R.id.txtCurrenttime);
	    _txtTotalRecord = (TextView)findViewById(R.id.txtTotalRecord);
	    startClock();
	    //_txtCurrentTime.setText(_sdfWatchTime.format(new Date()));
	    initAutoSave();
	    LoadSetting();
	    _txtTotalRecord.setText(String.valueOf(_data.size()));
	    // initAutoSend();
	    //String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
	    _txtCurrentDate = (TextView) findViewById(R.id.txtCurrentDate);
	    _txtCurrentDate.setText(sdf.format(new Date(System.currentTimeMillis())));
	    //_txtCurrentDate.setText(android.text.format.DateFormat.getDateFormat().format(new Date()));

    }

	private String getTime() {
		return stf.format(new Date(System.currentTimeMillis()));
    }
	private void LoadSetting()
	{
		int hour = _prefs.getInt(_keyPrefix.concat(".sendhour"),8);
		int minute = _prefs.getInt(_keyPrefix.concat(".sendminute"),0);
		int hour2 = _prefs.getInt(_keyPrefix.concat(".sendhour2"),9);
		int minute2 = _prefs.getInt(_keyPrefix.concat(".sendminute2"),30);
		int hour3 = _prefs.getInt(_keyPrefix.concat(".sendhour3"),10);
		int minute3 = _prefs.getInt(_keyPrefix.concat(".sendminute3"),45);
	    /* fill in edit text field */
		_terminalId = _prefs.getInt(_keyPrefix.concat(".terminalid"), _defaultTerminalId);
		_maxDigit = _prefs.getInt(_keyPrefix.concat(".maxdigits"), _defaultMaxDigit);
		_secretCode = _prefs.getString(_keyPrefix.concat(".secretcode"), _defaultSecretCode);
		_manualSendCode = _prefs.getString(_keyPrefix.concat(".manualsendcode"), _defaultManualSendCode);
		_manualClearData = _prefs.getString(_keyPrefix.concat(".manualcleardata"), _defaultManualClearData);
		_autoSaveCountDown = _prefs.getInt(_keyPrefix.concat(".autosavecountdown"),_defaultAutoSaveTime) * 1000;
		_autoSendTime = new Time(((hour*60)+minute)*1000);
		_autoSendTime2 = new Time(((hour2*60)+minute2)*1000);
		_autoSendTime3 = new Time(((hour3*60)+minute3)*1000);
		_serverPath = _prefs.getString(_keyPrefix.concat(".serverurl"), _defaultServerPath);
		_data = _prefs.getStringSet(_keyPrefix.concat(".data"), new HashSet<String>());

        Calendar calendar = Calendar.getInstance();
		long currentTimestamp = calendar.getTimeInMillis();

        calendar.set(Calendar.HOUR_OF_DAY, hour);
        calendar.set(Calendar.MINUTE, minute);
        calendar.set(Calendar.SECOND, 0);
		long diffTimestamp = calendar.getTimeInMillis() - currentTimestamp;
		long autoSendTimeDelay = (diffTimestamp < 0 ? 0 : diffTimestamp);
		Log.d("count down auto send",String.valueOf(autoSendTimeDelay).concat(" ms "));
		calendar.set(Calendar.HOUR_OF_DAY, hour2);
		calendar.set(Calendar.MINUTE, minute2);
		calendar.set(Calendar.SECOND, 0);
		long diffTimestamp2 = calendar.getTimeInMillis() - currentTimestamp;
		long autoSendTimeDelay2 = (diffTimestamp2 < 0 ? 0 : diffTimestamp2);
		Log.d("count down auto send",String.valueOf(autoSendTimeDelay2).concat(" ms "));

		calendar.set(Calendar.HOUR_OF_DAY, hour3);
		calendar.set(Calendar.MINUTE, minute3);
		calendar.set(Calendar.SECOND, 0);
		long diffTimestamp3 = calendar.getTimeInMillis() - currentTimestamp;
		long autoSendTimeDelay3 = (diffTimestamp3 < 0 ? 0 : diffTimestamp3);
		Log.d("count down auto send",String.valueOf(autoSendTimeDelay3).concat(" ms "));
		/* initail auto send */
		if( autoSendTimeDelay > 0)
			new Handler().postDelayed(AutoSendTimeTask, autoSendTimeDelay);
		if( autoSendTimeDelay2 > 0)
			new Handler().postDelayed(AutoSendTimeTask, autoSendTimeDelay2);
		if( autoSendTimeDelay3 > 0)
			new Handler().postDelayed(AutoSendTimeTask, autoSendTimeDelay3);

	}

//	@Override
//	protected  void onStart()
//	{
//		super.onStart();
//		Log.i("process","Activity Started");
//		_broadcastReceiver = new BroadcastReceiver() {
//			@Override
//			public void onReceive(Context context, Intent intent) {
//				if( intent.getAction().compareTo(Intent.ACTION_TIME_TICK) == 1)
//				{
//					_txtCurrentTime.setText(_sdfWatchTime.format(new Date()));
//				}
//			}
//		};
//		registerReceiver(_broadcastReceiver,new IntentFilter(Intent.ACTION_TIME_TICK));
//
//	}
//	@Override
//	protected void onStop()
//	{
//		super.onStop();
//		if( _broadcastReceiver != null)
//			unregisterReceiver(_broadcastReceiver);
//	}
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
       // getMenuInflater().inflate(R.menu.main, menu);
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//        if (id == R.id.action_settings) {
//            return true;
//        }
        return super.onOptionsItemSelected(item);
    }
	private void appendText(String str)
	{
		/* if showed text length is more than _maxDigit characters, avoid this event */
		String oldText =_txtShowSID.getText().toString();
		if( oldText.length() < _maxDigit )
		{
			_txtShowSID.setText(oldText.concat(str));
			startAutoSave();
		}
		Log.d("click",str+","+oldText+","+_txtShowSID.getText());
	}
	private void clearText()
	{
		_txtShowSID.setText("");
		_txtShowSID.setTextColor(0xff24a4ff);
		if( _autoSave != null) {
			_autoSave.cancel();
			_autoSave = null;
		}

	}
	private void initAutoSave()
	{
		Log.d("auto-save","init auto-save");
		_autoSave = new CountDownTimer(_autoSaveCountDown,_countDownInterval) {
			@Override
			public void onTick(long l) {

			}

			@Override
			public void onFinish() {
				saveTime();
			}
		};
	}
	private void startAutoSave()
	{

		if( _autoSave != null )
		{
			Log.d("auto-save","restart auto-save");
			_autoSave.cancel();
			_autoSave = null;
		}
		initAutoSave();
		_autoSave.start();
}
	private void setAutoSave()
	{
		//_autoSaveCountDown = ;
		if( _autoSave != null)
		{
			_autoSave.cancel();
			_autoSave = null;
		}
		initAutoSave();
		System.gc();
	}
	private void initAutoSend()
	{
		Log.d("auto-send","init auto-send");
		// _autoSend = Timer
		_autoSend = new Timer();
		_autoSend.schedule(new TimerTask() {
			@Override
			public void run() {
				_trySend = _maxTrySend;
				sendTime();
			}
		},_autoSendTime);
		_autoSend2 = new Timer();
		_autoSend2.schedule(new TimerTask() {
			@Override
			public void run() {
				_trySend = _maxTrySend;
				sendTime();
			}
		},_autoSendTime2);
		_autoSend3 = new Timer();
		_autoSend3.schedule(new TimerTask() {
			@Override
			public void run() {
				_trySend = _maxTrySend;
				sendTime();
			}
		},_autoSendTime3);
	}
	private void setAutoSend()
	{
		Log.d("auto-send","auto send old : " + _autoSendTime);
		//_autoSendTime = ;
		Log.d("auto-send","auto send new : " + _autoSendTime);
		if( _autoSend != null)
		{
			_autoSend.cancel();
			_autoSend = null;
		}
		initAutoSend();
		System.gc();

	}
	private String prepareSendTime()
	{
		String prepare = "";
		boolean first = true;
		for (String str : _data) {
			if( !first ){
				prepare = prepare.concat(",");
			}
			prepare = prepare.concat(str);
			first = false;
		}
		return prepare;
	}
	private void sendTime()
	{
		/* fetch data and convert to json */
		Log.d("send time","send data to server ");
		// prepare
		_trySend = _maxTrySend;
		asyncTask = new TheTask();
		asyncTask.delegate = this;
		asyncTask.execute(_serverPath,prepareSendTime());


		/* send data to server */
		/* clear data */

		System.gc();
	}
	public void processFinish(String output){
		Log.d("response",output);
		if( output.toString().compareTo("OK") == 0)
		{
			clearData();
			_txtTotalRecord.setText("Data sent");
		}
		if( output.toString().compareTo("ERR") == 0 && _trySend != 0 )
		{
			Log.d("retry send","send time round ".concat(String.valueOf(_trySend)));
			asyncTask = new TheTask();
			asyncTask.delegate = this;
			asyncTask.execute(_serverPath,prepareSendTime());
			_trySend--;
		}
	}


	private void saveTime()
	{
		Log.d("auto-save","enter save time");
		if( _txtShowSID.getText().length() == 0)
			return;
		/* if input value is _maxDigit digit and not secret code, let add sid,ctime to data */
		if( _txtShowSID.getText().length() == _maxDigit && _txtShowSID.getText().toString().indexOf('*') == -1) {
//			Map<String, Object> temp = new HashMap<String, Object>();
//			Date date = new Date();
//			temp.put("id", );
//			temp.put("time", new java.sql.Timestamp(date.getTime()));
//			if( _data == null)
//			{
//				_data = new ArrayList<Map>();
//			}
//			_data.add(temp);
			Date date = new Date();
			//java.sql.Timestamp timestamp = new java.sql.Timestamp(date.getTime());
			SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
			String temp = new String(_txtShowSID.getText().toString().concat("|").concat(format.format(date)));
			if( _data == null)
			{
				clearData();
			}
			_data.add(temp);
			_prefs.edit().putStringSet(_keyPrefix.concat(".data"), _data).commit();
			Log.d("auto-save", "save time to variable");
			_txtTotalRecord.setText(String.valueOf(_data.size()));
			clearText();
		}
		else if( _txtShowSID.getText().toString().compareTo(_secretCode) == 0)
		{
			showSettings();
			clearText();
		}
		else if( _txtShowSID.getText().toString().compareTo(_manualClearData) == 0)
		{
			clearData();
			_prefs.edit().putStringSet(_keyPrefix.concat(".data"), _data).commit();
			System.gc();
			clearText();
		}
		else if( _txtShowSID.getText().toString().compareTo(_manualSendCode) == 0)
		{
			_trySend = _maxTrySend;
			sendTime();
			clearText();
		}
		else if( _txtShowSID.getText().toString().compareTo(_resetCode) == 0)
		{
			// clear all config to default
			_prefs.edit().putString(_keyPrefix.concat(".manualcleardata"),_defaultManualClearData).commit();
			_prefs.edit().putString(_keyPrefix.concat(".manualsendcode"),_defaultManualSendCode).commit();
			_prefs.edit().putString(_keyPrefix.concat(".secretcode"),_defaultSecretCode).commit();
			clearText();
			LoadSetting();
		}
		else if( _txtShowSID.getText().toString().compareTo(_factoryReset) == 0)
		{
			// factory reset
			_prefs.edit().putString(_keyPrefix.concat(".serverurl"),_defaultServerPath).commit();
			_prefs.edit().putString(_keyPrefix.concat(".secretcode"),_defaultSecretCode).commit();
			_prefs.edit().putInt(_keyPrefix.concat(".maxdigits"),_defaultMaxDigit).commit();
			_prefs.edit().putInt(_keyPrefix.concat(".sendhour"),8).commit();
			_prefs.edit().putInt(_keyPrefix.concat(".sendminute"),0).commit();
			_prefs.edit().putInt(_keyPrefix.concat(".sendhour2"),9).commit();
			_prefs.edit().putInt(_keyPrefix.concat(".sendminute2"),30).commit();
			_prefs.edit().putInt(_keyPrefix.concat(".sendhour3"),10).commit();
			_prefs.edit().putInt(_keyPrefix.concat(".sendminute3"),45).commit();
			_prefs.edit().putString(_keyPrefix.concat(".manualcleardata"),_defaultManualClearData).commit();
			_prefs.edit().putString(_keyPrefix.concat(".manualsendcode"),_defaultManualSendCode).commit();
			_prefs.edit().putInt(_keyPrefix.concat(".autosavecountdown"),_defaultAutoSaveTime).commit();
			_prefs.edit().putStringSet(_keyPrefix.concat(".data"),new HashSet<String>()).commit();
			clearText();
			LoadSetting();
		}
		else
		{
			_txtShowSID.setTextColor(0xffc80011);
			_shock = true;
		}

	}
	public void showSettings()
	{
		Log.d("setting","show setting");
		Intent intent = new Intent(this,SettingActivity.class);
		startActivity(intent);

	}
	public void saveSettings()
	{
		Log.d("setting","save setting");
		Log.d("setting","server path : " + _serverPath);
		Log.d("setting","secret code : " + _secretCode);
		Log.d("setting","auto save count down time : " + _autoSaveCountDown);
		Log.d("setting","auto send time : " + _autoSendTime);

	}
	public void hideSettings()
	{
		Log.d("setting","hide setting");
		clearText();
	}
	public void onBtnClicked(View v){
		if( _shock )
		{
			switch (v.getId()) {
				case R.id.btnNumpadClear:
					this.clearText();
					_shock = false;
					break;
			}
		}
		else {
			switch (v.getId()) {
				case R.id.btnNumpad0:
					this.appendText("0");
					break;
				case R.id.btnNumpad1:
					this.appendText("1");
					break;
				case R.id.btnNumpad2:
					this.appendText("2");
					break;
				case R.id.btnNumpad3:
					this.appendText("3");
					break;
				case R.id.btnNumpad4:
					this.appendText("4");
					break;
				case R.id.btnNumpad5:
					this.appendText("5");
					break;
				case R.id.btnNumpad6:
					this.appendText("6");
					break;
				case R.id.btnNumpad7:
					this.appendText("7");
					break;
				case R.id.btnNumpad8:
					this.appendText("8");
					break;
				case R.id.btnNumpad9:
					this.appendText("9");
					break;
				case R.id.btnNumpadStar:
					this.appendText("*");
					break;
				case R.id.btnNumpadClear:
					this.clearText();
					break;
				case R.id.btnCheckIn:
					this.saveTime();
			}
		}
	}
	private void clearData()
	{
		_data = new HashSet<String>();
		_txtTotalRecord.setText("0");
	}
}