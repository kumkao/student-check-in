package kanchaistudio.studentcheckin;

import android.app.Activity;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TimePicker;
import android.content.SharedPreferences;
import android.content.Context;
import kanchaistudio.studentcheckin.R;

public class SettingActivity extends Activity {
	private final String _factoryReset = "*536*";
	private final String _resetCode = "*533*";
	private final Integer _defaultTerminalId = 0;
	private final Integer _defaultMaxDigit = 5;
	private final String _defaultSecretCode = "*999*";
	private final String _defaultManualSendCode = "*998*";
	private final String _defaultManualClearData = "*997*";
	private final Integer _defaultAutoSaveTime = 10;
	private final String _defaultServerPath = "";
	private TimePicker timePicker;
	private Integer _hour,_hour2,_hour3;
	private Integer _minute,_minute2,_minute3;

	private EditText _editTextSendTime = null;
	private EditText _editTextSendTime2 = null;
	private EditText _editTextSendTime3 = null;
	private EditText _editTextMaxDigits = null;
	private EditText _editTextSecretCode = null;
	private EditText _editTextServerUrl = null;
	private EditText _editTextManualSendCode = null;
	private EditText _editTextManualClearData = null;
	private EditText _editTextAutoSaveCountDown = null;
	private EditText _editTextTerminalId = null;
	final String _keyPrefix = "kanchai.studentcheckin.app";
	private SharedPreferences _prefs;

	private TimePickerDialog.OnTimeSetListener timePickerListener =  new TimePickerDialog.OnTimeSetListener() {
		public void onTimeSet(TimePicker view, int selectedHour, int selectedMinute) {
			_hour = selectedHour;
			_minute = selectedMinute;
			// set current time into textview
			_editTextSendTime.setText(new StringBuilder().append(padding_str(_hour)).append(":").append(padding_str(_minute)));
		}
	};
	private TimePickerDialog.OnTimeSetListener timePickerListener2 =  new TimePickerDialog.OnTimeSetListener() {
		public void onTimeSet(TimePicker view, int selectedHour, int selectedMinute) {
			_hour2 = selectedHour;
			_minute2 = selectedMinute;
			// set current time into textview
			_editTextSendTime2.setText(new StringBuilder().append(padding_str(_hour2)).append(":").append(padding_str(_minute2)));
		}
	};
	private TimePickerDialog.OnTimeSetListener timePickerListener3 =  new TimePickerDialog.OnTimeSetListener() {
		public void onTimeSet(TimePicker view, int selectedHour, int selectedMinute) {
			_hour3 = selectedHour;
			_minute3 = selectedMinute;
			// set current time into textview
			_editTextSendTime3.setText(new StringBuilder().append(padding_str(_hour3)).append(":").append(padding_str(_minute3)));
		}
	};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
	    _prefs = this.getSharedPreferences(_keyPrefix, Context.MODE_PRIVATE);
        super.onCreate(savedInstanceState);
	    this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_setting);

	    /* get view */
	    _editTextTerminalId = (EditText)findViewById(R.id.editTextTerminalId);
	    _editTextSendTime = (EditText)findViewById(R.id.editTextSendTime);
	    _editTextSendTime2 = (EditText)findViewById(R.id.editTextSendTime2);
	    _editTextSendTime3 = (EditText)findViewById(R.id.editTextSendTime3);
	    _editTextMaxDigits = (EditText)findViewById(R.id.editTextMaxDigits);
	    _editTextSecretCode = (EditText)findViewById(R.id.editTextSecretCode);
	    _editTextServerUrl = (EditText)findViewById(R.id.editTextServerUrl);
	    _editTextManualSendCode = (EditText)findViewById(R.id.editTextManualSendCode);
		_editTextManualClearData = (EditText)findViewById(R.id.editTextManualClearData);
	    _editTextAutoSaveCountDown = (EditText)findViewById(R.id.editTextAutoSaveCountDown);
	    /* load setting */
		_hour = _prefs.getInt(_keyPrefix.concat(".sendhour"),8);
	    _minute = _prefs.getInt(_keyPrefix.concat(".sendminute"),0);
	    _hour2 = _prefs.getInt(_keyPrefix.concat(".sendhour2"),9);
	    _minute2 = _prefs.getInt(_keyPrefix.concat(".sendminute2"),30);
	    _hour3 = _prefs.getInt(_keyPrefix.concat(".sendhour3"),10);
	    _minute3 = _prefs.getInt(_keyPrefix.concat(".sendminute3"),45);
	    /* fill in edit text field */
	    _editTextTerminalId.setHint(((Integer)_prefs.getInt(_keyPrefix.concat(".terminalid"),_defaultTerminalId)).toString());
	    _editTextMaxDigits.setHint(((Integer)_prefs.getInt(_keyPrefix.concat(".maxdigits"),_defaultMaxDigit)).toString());
	    _editTextSecretCode.setHint(_prefs.getString(_keyPrefix.concat(".secretcode"),_defaultSecretCode));
	    _editTextSendTime.setHint(String.format("%02d",_hour).concat(":".concat(String.format("%02d",_minute))));
	    _editTextSendTime2.setHint(String.format("%02d",_hour2).concat(":".concat(String.format("%02d",_minute2))));
	    _editTextSendTime3.setHint(String.format("%02d",_hour3).concat(":".concat(String.format("%02d",_minute3))));
	    _editTextServerUrl.setHint(_prefs.getString(_keyPrefix.concat(".serverurl"), "http://127.0.0.1:80"));
	    _editTextManualSendCode.setHint(_prefs.getString(_keyPrefix.concat(".manualsendcode"), _defaultManualSendCode));
	    _editTextManualClearData.setHint(_prefs.getString(_keyPrefix.concat(".manualcleardata"), _defaultManualSendCode));
	    _editTextAutoSaveCountDown.setHint(((Integer)_prefs.getInt(_keyPrefix.concat(".autosavecountdown"),_defaultAutoSaveTime)).toString());
    }
	public void ShowTimePickerDialog(View v)
	{
		showDialog(999);
	}
	public void ShowTimePickerDialog2(View v)
	{
		showDialog(998);
	}
	public void ShowTimePickerDialog3(View v)
	{
		showDialog(997);
	}
	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
			case 999:
				// set time picker as current time
				return new TimePickerDialog(this, timePickerListener, _hour, _minute,true);
			case 998:
				// set time picker as current time
				return new TimePickerDialog(this, timePickerListener2, _hour2, _minute2,true);
			case 997:
				// set time picker as current time
				return new TimePickerDialog(this, timePickerListener3, _hour3, _minute3,true);
		}
		return null;
	}

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.setting, menu);
        return true;
    }

	private static String padding_str(int c) {
		if (c >= 10)
			return String.valueOf(c);
		else
			return "0" + String.valueOf(c);
	}
	public void UpdateSetting(View v)
	{
		if( _editTextServerUrl.getText().toString().compareTo("") != 0 )
		{
			_prefs.edit().putString(_keyPrefix.concat(".serverurl"),_editTextServerUrl.getText().toString()).commit();
		}
		if( _editTextSecretCode.getText().toString().compareTo("") != 0 )
		{
			_prefs.edit().putString(_keyPrefix.concat(".secretcode"),_editTextSecretCode.getText().toString()).commit();
		}
		if( _editTextMaxDigits.getText().toString().compareTo("") != 0 )
		{
			Integer maxdigit;
			try
			{
				maxdigit = Integer.parseInt(_editTextMaxDigits.getText().toString());
			}
			catch (Exception e)
			{
				_editTextMaxDigits.requestFocus ();
				return;
			}
			_prefs.edit().putInt(_keyPrefix.concat(".maxdigits"),maxdigit).commit();
		}
		if( _editTextTerminalId.getText().toString().compareTo("") != 0 )
		{
			Integer terminalId;
			try
			{
				terminalId = Integer.parseInt(_editTextTerminalId.getText().toString());
			}
			catch (Exception e)
			{
				_editTextTerminalId.requestFocus ();
				return;
			}
			_prefs.edit().putInt(_keyPrefix.concat(".terminalid"),terminalId).commit();
		}
		if( _editTextSendTime.getText().toString().compareTo("") != 0 )
		{
			_prefs.edit().putInt(_keyPrefix.concat(".sendhour"),_hour).commit();
			_prefs.edit().putInt(_keyPrefix.concat(".sendminute"),_minute).commit();
		}
		if( _editTextSendTime2.getText().toString().compareTo("") != 0 )
		{
			_prefs.edit().putInt(_keyPrefix.concat(".sendhour2"),_hour2).commit();
			_prefs.edit().putInt(_keyPrefix.concat(".sendminute2"),_minute2).commit();
		}
		if( _editTextSendTime3.getText().toString().compareTo("") != 0 )
		{
			_prefs.edit().putInt(_keyPrefix.concat(".sendhour3"),_hour3).commit();
			_prefs.edit().putInt(_keyPrefix.concat(".sendminute3"),_minute3).commit();
		}
		if( _editTextManualClearData.getText().toString().compareTo("") != 0 )
		{
			_prefs.edit().putString(_keyPrefix.concat(".manualcleardata"),_editTextManualClearData.getText().toString()).commit();
		}
		if( _editTextManualSendCode.getText().toString().compareTo("") != 0 )
		{
			_prefs.edit().putString(_keyPrefix.concat(".manualsendcode"),_editTextManualSendCode.getText().toString()).commit();
		}
		if( _editTextAutoSaveCountDown.getText().toString().compareTo("") != 0 )
		{
			Integer countdown;
			try
			{
				countdown = Integer.parseInt(_editTextAutoSaveCountDown.getText().toString());
			}
			catch (Exception e)
			{
				_editTextAutoSaveCountDown.requestFocus ();
				return;
			}
			_prefs.edit().putInt(_keyPrefix.concat(".autosavecountdown"),countdown).commit();
		}
		/* back to main activity after save changed */
		Intent intent = new Intent(this,Main.class);
		startActivity(intent);
	}
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//        if (id == R.id.action_settings) {
//            return true;
//        }
//        return super.onOptionsItemSelected(item);
//    }
}
